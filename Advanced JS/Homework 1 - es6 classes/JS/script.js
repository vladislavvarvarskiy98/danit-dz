"use strict";

class Employee{
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    };

    get name(){
        return this._name;
    };

    set name(value){
        this._name = value;
    };

    get age(){
        return this._age;
    };

    set age(value){
        this._age = value;
    };

    get salary(){
        return this._salary;
    };

    set salary(value){
        this._salary = value;
    };
};

const employee = new Employee("Vlad", 23, 20000);
console.log(employee);

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    };

    get salary(){
        return this._salary * 3;
    };

    set salary(value){
        this._salary = value;
    };
    
};

const programmer = new Programmer("Mykhailo", 25, 30000, ["eng", "urk", "rus"]);
const seconProgrammer = new Programmer("Alexandr", 21, 22000, ["eng", "rus"]);
const thirdProgrammer = new Programmer("Illia", 23, 25000, ["ukr", "rus"]);
console.log(programmer);
console.log(seconProgrammer);
console.log(thirdProgrammer);