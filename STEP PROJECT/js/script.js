"use strict";


//OUR SERVICES
let links = document.getElementsByClassName("our-services-menu-link");
let undermenu = document.getElementsByClassName("our-services-undermenu")

for(let i = 0; i < links.length; i++){
    
    function onClickLinksHandler(event){
        if(this.className === "our-services-menu-link"){
            for(let elem of links){
                elem.className = "our-services-menu-link";
            };
            this.className = "our-services-menu-link our-services-menu-link-active";
        }

        if(this.parentNode.className === "our-services-menu-item"){
            for(let elem of links){
                elem.parentNode.className = "our-services-menu-item";
            };
            this.parentNode.className = "our-services-menu-item our-services-menu-item-active";
        }
        if(this.nextElementSibling.className === "our-services-menu-triangle"){
            for(let elem of links){
                elem.nextElementSibling.style = "display: none";
            };
            this.nextElementSibling.style = "display: block";
        }

        if(this.dataset.property === "web"){
            for(let elem of undermenu){
                elem.style = "display: none";
                if(elem.dataset.property === "web"){
                    elem.style = "display: flex";
                };   
            }
        }

        if(this.dataset.property === "graphic"){
            for(let elem of undermenu){
                elem.style = "display: none";
                if(elem.dataset.property === "graphic"){
                    elem.style = "display: flex";
                };   
            }
        }

        if(this.dataset.property === "support"){
            for(let elem of undermenu){
                elem.style = "display: none";
                if(elem.dataset.property === "support"){
                    elem.style = "display: flex";
                };   
            }
        }

        if(this.dataset.property === "app"){
            for(let elem of undermenu){
                elem.style = "display: none";
                if(elem.dataset.property === "app"){
                    elem.style = "display: flex";
                };   
            }
        }

        if(this.dataset.property === "marketing"){
            for(let elem of undermenu){
                elem.style = "display: none";
                if(elem.dataset.property === "marketing"){
                    elem.style = "display: flex";
                };   
            }
        }

        if(this.dataset.property === "seo"){
            for(let elem of undermenu){
                elem.style = "display: none";
                if(elem.dataset.property === "seo"){
                    elem.style = "display: flex";
                };   
            }
        }
    }


    links[i].addEventListener("click", onClickLinksHandler);
}


//AMAZING WORK

let amazingWorkItems = document.getElementsByClassName("amazing-work-menu-item");
let amazingWorkLinks = document.getElementsByClassName("amazing-work-link");
let amazingWorkImages = document.getElementsByClassName("amazing-work-item");
let amazingWorkButton = document.getElementById("amazing-work-button");
let amazingWorkImagesWrapper = document.getElementById("amazing-work-images-wrapper")

for(let i = 0; i < amazingWorkLinks.length; i++){

    function onClickItemsHandler(){
        if(this.className === "amazing-work-link"){
            for(let elem of amazingWorkLinks){
                elem.className = "amazing-work-link";
            };
            this.className = "amazing-work-link amazing-work-link-active";
        };

        if(this.parentNode.className === "amazing-work-menu-item"){

            for(let elem of amazingWorkItems){
                elem.className = "amazing-work-menu-item";
                console.log(elem);
            };
            this.parentNode.className = "amazing-work-menu-item amazing-work-menu-item-active"
        }

        if(this.dataset.property === "all"){
            for(let elem of amazingWorkImages){
                elem.style = "display: block";
            }
        }

        if(this.dataset.property === "graphic"){
            for(let elem of amazingWorkImages){
                elem.style = "display: none"
                if(elem.dataset.property === "graphic"){
                    elem.style = "display: block";
                }; 
            }
        }

        if(this.dataset.property === "web"){
            for(let elem of amazingWorkImages){
                elem.style = "display: none"
                if(elem.dataset.property === "web"){
                    elem.style = "display: block";
                }; 
            }
        }

        if(this.dataset.property === "landing"){
            for(let elem of amazingWorkImages){
                elem.style = "display: none"
                if(elem.dataset.property === "landing"){
                    elem.style = "display: block";
                }; 
            }
        }

        if(this.dataset.property === "wordpress"){
            for(let elem of amazingWorkImages){
                elem.style = "display: none"
                if(elem.dataset.property === "wordpress"){
                    elem.style = "display: block";
                }; 
            }
        }

    }

    amazingWorkLinks[i].addEventListener("click", onClickItemsHandler);
}



function onButtonClickHandler(){
    for(let i = 13; i < 25; i++){
        let divItem = document.createElement("div");
        let img = document.createElement("img");
        let divDescription = document.createElement("div");
        let linkIcon = document.createElement("a")
        let linkImg = document.createElement("img");
        let upperLink = document.createElement("a");
        let lowerLink = document.createElement("a")
        
        img.src = `./images/Section our amazing work/Layer ${i}.png`;
        divItem.className = "amazing-work-item amazing-work-item-new";

        if(i >= 13 && i <= 15){
            divItem.setAttribute("data-property", "graphic")
            }
        if(i >= 16 && i <= 18){
            divItem.setAttribute("data-property", "web")
        }
        if(i >= 19 && i <= 21){
            divItem.setAttribute("data-property", "landing")
        }
        if(i >= 22 && i <= 24){
            divItem.setAttribute("data-property", "wordpress")
        }

        divDescription.className = "amazing-work-image-description";
        linkIcon.className = "amazing-work-image-description-icon";
        linkIcon.href = "##"
        linkImg.src = "./images/Section our amazing work/icon.png"
        upperLink.className = "amazing-work-image-description-upperlink";
        upperLink.href = "##"
        upperLink.innerHTML = "creative design";
        lowerLink.className = "amazing-work-image-description-lowerlink";
        lowerLink.href = "##"
        linkIcon.append(linkImg);
        divDescription.append(linkIcon);
        divDescription.append(upperLink);
        divDescription.append(lowerLink);
        img.className = "amazing-work-image"
        divItem.append(img);
        divItem.append(divDescription)
        amazingWorkImagesWrapper.append(divItem)

        if(lowerLink.parentElement.parentElement.dataset.property === "graphic"){
            lowerLink.innerHTML = "Graphic Design";
        }
        if(lowerLink.parentElement.parentElement.dataset.property === "web"){
            lowerLink.innerHTML = "Web Design";
        }
        if(lowerLink.parentElement.parentElement.dataset.property === "landing"){
            lowerLink.innerHTML = "Landing Pages";
        }
        if(lowerLink.parentElement.parentElement.dataset.property === "wordpress"){
            lowerLink.innerHTML = "Wordpress";
        }
    }
    amazingWorkButton.style = "display: none"
    amazingWorkButton.removeEventListener("click", onButtonClickHandler);
}

amazingWorkButton.addEventListener("click", onButtonClickHandler);


//WHAT PEOPLE SAY

let whatPeopleSayLinks = document.getElementsByClassName("section-what-people-say-image-link");
let whatPeopleSayCircles = document.getElementsByClassName("section-what-people-say-image-link-circle");
let personName = document.getElementById("person-name");
let personPosition = document.getElementById("person-position");
let whatPeopleSayImage = document.getElementById("section-what-people-say-image");
let whatPeopleSayLeftButton = document.getElementById("section-what-people-say-left-button");
let whatPeopleSayRightButton = document.getElementById("section-what-people-say-right-button");
let whatPeopleSayDescription = document.getElementById("section-what-people-say-description");


for(let i = 0; i < whatPeopleSayLinks.length; i++){

    function onWhatPeopleSayLinksHandler(){
        
        for (let i = 0; i < 4; i++){
            if(this.dataset.person === "hasan"){

                //анимация кнопок

                for(let elem of whatPeopleSayLinks){
                    elem.className = "section-what-people-say-image-link";
                }
                this.className = "section-what-people-say-image-link section-what-people-say-image-link-active"

                for(let elem of whatPeopleSayCircles){
                    elem.className = "section-what-people-say-image-link-circle";
                }
                this.previousElementSibling.className = "section-what-people-say-image-link-circle section-what-people-say-image-link-circle-active";

                for(let elem of whatPeopleSayLinks){
                    if(elem.className === "section-what-people-say-image-link"){
                        elem.style = "top: 25px; transition-duration: 1s"
                    }
                }
                for(let elem of whatPeopleSayCircles){
                    if(elem.className === "section-what-people-say-image-link-circle"){
                        elem.style = "margin-bottom: 0; transition-duration: 1s";
                    }
                }
                
                this.style = "top: 5px; transition-duration: 1s"
                for(let i = 0; i < 4; i++){
                    if(this.previousElementSibling.dataset.person === "hasan"){
                        this.previousElementSibling.style = "margin-bottom: 40px; transition-duration: 1s" 
                    }
                }

                //меняем контент

                whatPeopleSayImage.src = "./images/Section what people say/Layer 1.png"
                personName.innerHTML = "Hasan Ali";
                personPosition.innerHTML ="UX Designer"
                whatPeopleSayDescription.innerHTML = "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis."
            }

            if(this.dataset.person === "mike"){

                //анимация кнопок

                for(let elem of whatPeopleSayLinks){
                    elem.className = "section-what-people-say-image-link";
                }
                this.className = "section-what-people-say-image-link section-what-people-say-image-link-active"

                for(let elem of whatPeopleSayCircles){
                    elem.className = "section-what-people-say-image-link-circle";
                }
                this.previousElementSibling.className = "section-what-people-say-image-link-circle section-what-people-say-image-link-circle-active";

                for(let elem of whatPeopleSayLinks){
                    if(elem.className === "section-what-people-say-image-link"){
                        elem.style = "top: 25px; transition-duration: 1s"
                    }
                }
                for(let elem of whatPeopleSayCircles){
                    if(elem.className === "section-what-people-say-image-link-circle"){
                        elem.style = "margin-bottom: 0; transition-duration: 1s";
                    }
                }
                
                this.style = "top: 5px; transition-duration: 1s"
                for(let i = 0; i < 4; i++){
                    if(this.previousElementSibling.dataset.person === "mike"){
                        this.previousElementSibling.style = "margin-bottom: 40px; transition-duration: 1s" 
                    }
                }

                //меняем контент

                whatPeopleSayImage.src = "./images/Section what people say/Layer 2.png";
                personName.innerHTML = "Mike Tompson";
                personPosition.innerHTML = "UX Developer"
                whatPeopleSayDescription.innerHTML = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis aliquam, placeat doloribus aperiam officiis repudiandae quibusdam illo, suscipit id, libero labore iste dolor odio est hic reiciendis eius sint quos! Quia accusantium magnam tenetur recusandae alias ducimus."

            }
            
            if(this.dataset.person === "sarah"){

                //анимация кнопок

                for(let elem of whatPeopleSayLinks){
                    elem.className = "section-what-people-say-image-link";
                }
                this.className = "section-what-people-say-image-link section-what-people-say-image-link-active"

                for(let elem of whatPeopleSayCircles){
                    elem.className = "section-what-people-say-image-link-circle";
                }
                this.previousElementSibling.className = "section-what-people-say-image-link-circle section-what-people-say-image-link-circle-active";

                for(let elem of whatPeopleSayLinks){
                    if(elem.className === "section-what-people-say-image-link"){
                        elem.style = "top: 25px; transition-duration: 1s"
                    }
                }
                for(let elem of whatPeopleSayCircles){
                    if(elem.className === "section-what-people-say-image-link-circle"){
                        elem.style = "margin-bottom: 0; transition-duration: 1s";
                    }
                }
                
                this.style = "top: 5px; transition-duration: 1s"
                for(let i = 0; i < 4; i++){
                    if(this.previousElementSibling.dataset.person === "sarah"){
                        this.previousElementSibling.style = "margin-bottom: 40px; transition-duration: 1s" 
                    }
                }

                //меняем контент

                whatPeopleSayImage.src = "./images/Section what people say/Layer 3.png";
                personName.innerHTML = "Sarah Mayer";
                personPosition.innerHTML ="Front-end Developer";
                whatPeopleSayDescription.innerHTML = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem reprehenderit deleniti optio beatae architecto aut sint quas officia soluta voluptate? Quis quod nulla perferendis!";

            }

            if(this.dataset.person === "jane"){

                //анимация кнопок

                for(let elem of whatPeopleSayLinks){
                    elem.className = "section-what-people-say-image-link";
                }
                this.className = "section-what-people-say-image-link section-what-people-say-image-link-active"

                for(let elem of whatPeopleSayCircles){
                    elem.className = "section-what-people-say-image-link-circle";
                }
                this.previousElementSibling.className = "section-what-people-say-image-link-circle section-what-people-say-image-link-circle-active";

                for(let elem of whatPeopleSayLinks){
                    if(elem.className === "section-what-people-say-image-link"){
                        elem.style = "top: 25px; transition-duration: 1s"
                    }
                }
                for(let elem of whatPeopleSayCircles){
                    if(elem.className === "section-what-people-say-image-link-circle"){
                        elem.style = "margin-bottom: 0; transition-duration: 1s";
                    }
                }
                
                this.style = "top: 5px; transition-duration: 1s"
                for(let i = 0; i < 4; i++){
                    if(this.previousElementSibling.dataset.person === "jane"){
                        this.previousElementSibling.style = "margin-bottom: 40px; transition-duration: 1s" 
                    }
                }

                //меняем контент

                whatPeopleSayImage.src = "./images/Section what people say/Layer 4.png"
                personName.innerHTML = "Jane Gordon";
                personPosition.innerHTML ="UI Designer";
                whatPeopleSayDescription.innerHTML = "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nihil consequuntur repudiandae perferendis earum quasi sed odio suscipit quisquam ad, eum temporibus recusandae excepturi. Fuga!";

            }
        }
    }

    whatPeopleSayLinks[i].addEventListener("click", onWhatPeopleSayLinksHandler);
}

function onWhatPeopleSayLeftButtonHandler(){
    for(let elem of whatPeopleSayLinks){
        if(!(elem.className === "section-what-people-say-image-link") && elem.dataset.person === "hasan"){
            for(let elem of whatPeopleSayLinks){
                elem.className = "section-what-people-say-image-link";
            }
            elem.style = "top: 25px; transition-duration: 1s";
            for(let elem of whatPeopleSayCircles){
                if(elem.dataset.person === "hasan"){
                    elem.className = "section-what-people-say-image-link-circle"
                    elem.style = "margin-bottom: 0; transition-duration: 1s";
                }
            }
            for(let elem of whatPeopleSayLinks){
                if(elem.dataset.person === "jane"){
                    elem.className = "section-what-people-say-image-link section-what-people-say-image-link-active"
                    elem.style = "top: 5px; transition-duration: 1s"
                    elem.previousElementSibling.className = "section-what-people-say-image-link-circle section-what-people-say-image-link-circle-active";
                    elem.previousElementSibling.style = "margin-bottom: 40px; transition-duration: 1s";
        
                }
            }

            whatPeopleSayImage.src = "./images/Section what people say/Layer 4.png";
            personName.innerHTML = "Jane Gordon";
            personPosition.innerHTML ="UI Designer";
            whatPeopleSayDescription.innerHTML = "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nihil consequuntur repudiandae perferendis earum quasi sed odio suscipit quisquam ad, eum temporibus recusandae excepturi. Fuga!";

            break
        }

        if(!(elem.className === "section-what-people-say-image-link") && elem.dataset.person === "jane"){
            for(let elem of whatPeopleSayLinks){
                elem.className = "section-what-people-say-image-link";
            }
            elem.style = "top: 25px; transition-duration: 1s";
            for(let elem of whatPeopleSayCircles){
                if(elem.dataset.person === "jane"){
                    elem.className = "section-what-people-say-image-link-circle"
                    elem.style = "margin-bottom: 0; transition-duration: 1s";
                }
            }
            for(let elem of whatPeopleSayLinks){
                if(elem.dataset.person === "sarah"){
                    elem.className = "section-what-people-say-image-link section-what-people-say-image-link-active"
                    elem.style = "top: 5px; transition-duration: 1s"
                    elem.previousElementSibling.className = "section-what-people-say-image-link-circle section-what-people-say-image-link-circle-active";
                    elem.previousElementSibling.style = "margin-bottom: 40px; transition-duration: 1s";
        
                }
            }

            whatPeopleSayImage.src = "./images/Section what people say/Layer 3.png";
            personName.innerHTML = "Sarah Mayer";
            personPosition.innerHTML ="Front-end Developer"
            whatPeopleSayDescription.innerHTML = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem reprehenderit deleniti optio beatae architecto aut sint quas officia soluta voluptate? Quis quod nulla perferendis!";

            break
        }

        if(!(elem.className === "section-what-people-say-image-link") && elem.dataset.person === "sarah"){
            for(let elem of whatPeopleSayLinks){
                elem.className = "section-what-people-say-image-link";
            }
            elem.style = "top: 25px; transition-duration: 1s";
            for(let elem of whatPeopleSayCircles){
                if(elem.dataset.person === "sarah"){
                    elem.className = "section-what-people-say-image-link-circle"
                    elem.style = "margin-bottom: 0; transition-duration: 1s";
                }
            }
            for(let elem of whatPeopleSayLinks){
                if(elem.dataset.person === "mike"){
                    elem.className = "section-what-people-say-image-link section-what-people-say-image-link-active"
                    elem.style = "top: 5px; transition-duration: 1s"
                    elem.previousElementSibling.className = "section-what-people-say-image-link-circle section-what-people-say-image-link-circle-active";
                    elem.previousElementSibling.style = "margin-bottom: 40px; transition-duration: 1s";
        
                }
            }

            whatPeopleSayImage.src = "./images/Section what people say/Layer 2.png";
            personName.innerHTML = "Mike Tompson";
            personPosition.innerHTML ="UX Developer"
            whatPeopleSayDescription.innerHTML = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis aliquam, placeat doloribus aperiam officiis repudiandae quibusdam illo, suscipit id, libero labore iste dolor odio est hic reiciendis eius sint quos! Quia accusantium magnam tenetur recusandae alias ducimus."

            break
        }

        if(!(elem.className === "section-what-people-say-image-link") && elem.dataset.person === "mike"){
            for(let elem of whatPeopleSayLinks){
                elem.className = "section-what-people-say-image-link";
            }
            elem.style = "top: 25px; transition-duration: 1s";
            for(let elem of whatPeopleSayCircles){
                if(elem.dataset.person === "mike"){
                    elem.className = "section-what-people-say-image-link-circle"
                    elem.style = "margin-bottom: 0; transition-duration: 1s";
                }
            }
            for(let elem of whatPeopleSayLinks){
                if(elem.dataset.person === "hasan"){
                    elem.className = "section-what-people-say-image-link section-what-people-say-image-link-active"
                    elem.style = "top: 5px; transition-duration: 1s"
                    elem.previousElementSibling.className = "section-what-people-say-image-link-circle section-what-people-say-image-link-circle-active";
                    elem.previousElementSibling.style = "margin-bottom: 40px; transition-duration: 1s";
        
                }
            }

            whatPeopleSayImage.src = "./images/Section what people say/Layer 1.png"
            personName.innerHTML = "Hasan Ali";
            personPosition.innerHTML ="UX Designer"
            whatPeopleSayDescription.innerHTML = "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis."

            break
        }
    }
}

whatPeopleSayLeftButton.addEventListener("click", onWhatPeopleSayLeftButtonHandler);


function onWhatPeopleSayRightButtonHandler(){
    for(let elem of whatPeopleSayLinks){
        if(!(elem.className === "section-what-people-say-image-link") && elem.dataset.person === "hasan"){
            for(let elem of whatPeopleSayLinks){
                elem.className = "section-what-people-say-image-link";
            }
            elem.style = "top: 25px; transition-duration: 1s";
            for(let elem of whatPeopleSayCircles){
                if(elem.dataset.person === "hasan"){
                    elem.className = "section-what-people-say-image-link-circle"
                    elem.style = "margin-bottom: 0; transition-duration: 1s";
                }
            }
            for(let elem of whatPeopleSayLinks){
                if(elem.dataset.person === "mike"){
                    elem.className = "section-what-people-say-image-link section-what-people-say-image-link-active"
                    elem.style = "top: 5px; transition-duration: 1s"
                    elem.previousElementSibling.className = "section-what-people-say-image-link-circle section-what-people-say-image-link-circle-active";
                    elem.previousElementSibling.style = "margin-bottom: 40px; transition-duration: 1s";
        
                }
            }

            whatPeopleSayImage.src = "./images/Section what people say/Layer 2.png";
            personName.innerHTML = "Mike Tompson";
            personPosition.innerHTML ="UX Developer";
            whatPeopleSayDescription.innerHTML = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis aliquam, placeat doloribus aperiam officiis repudiandae quibusdam illo, suscipit id, libero labore iste dolor odio est hic reiciendis eius sint quos! Quia accusantium magnam tenetur recusandae alias ducimus.";    

            break
        }

        if(!(elem.className === "section-what-people-say-image-link") && elem.dataset.person === "mike"){
            for(let elem of whatPeopleSayLinks){
                elem.className = "section-what-people-say-image-link";
            }
            elem.style = "top: 25px; transition-duration: 1s";
            for(let elem of whatPeopleSayCircles){
                if(elem.dataset.person === "mike"){
                    elem.className = "section-what-people-say-image-link-circle"
                    elem.style = "margin-bottom: 0; transition-duration: 1s";
                }
            }
            for(let elem of whatPeopleSayLinks){
                if(elem.dataset.person === "sarah"){
                    elem.className = "section-what-people-say-image-link section-what-people-say-image-link-active"
                    elem.style = "top: 5px; transition-duration: 1s"
                    elem.previousElementSibling.className = "section-what-people-say-image-link-circle section-what-people-say-image-link-circle-active";
                    elem.previousElementSibling.style = "margin-bottom: 40px; transition-duration: 1s";
        
                }
            }

            whatPeopleSayImage.src = "./images/Section what people say/Layer 3.png";
            personName.innerHTML = "Sarah Mayer";
            personPosition.innerHTML ="Front-end Developer";
            whatPeopleSayDescription.innerHTML = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem reprehenderit deleniti optio beatae architecto aut sint quas officia soluta voluptate? Quis quod nulla perferendis!";

            break
        }

        if(!(elem.className === "section-what-people-say-image-link") && elem.dataset.person === "sarah"){
            for(let elem of whatPeopleSayLinks){
                elem.className = "section-what-people-say-image-link";
            }
            elem.style = "top: 25px; transition-duration: 1s";
            for(let elem of whatPeopleSayCircles){
                if(elem.dataset.person === "sarah"){
                    elem.className = "section-what-people-say-image-link-circle"
                    elem.style = "margin-bottom: 0; transition-duration: 1s";
                }
            }
            for(let elem of whatPeopleSayLinks){
                if(elem.dataset.person === "jane"){
                    elem.className = "section-what-people-say-image-link section-what-people-say-image-link-active"
                    elem.style = "top: 5px; transition-duration: 1s"
                    elem.previousElementSibling.className = "section-what-people-say-image-link-circle section-what-people-say-image-link-circle-active";
                    elem.previousElementSibling.style = "margin-bottom: 40px; transition-duration: 1s";
        
                }
            }

            whatPeopleSayImage.src = "./images/Section what people say/Layer 4.png"
            personName.innerHTML = "Jane Gordon";
            personPosition.innerHTML ="UI Designer";
            whatPeopleSayDescription.innerHTML = "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nihil consequuntur repudiandae perferendis earum quasi sed odio suscipit quisquam ad, eum temporibus recusandae excepturi. Fuga!";

            break
        }

        if(!(elem.className === "section-what-people-say-image-link") && elem.dataset.person === "jane"){
            for(let elem of whatPeopleSayLinks){
                elem.className = "section-what-people-say-image-link";
            }
            elem.style = "top: 25px; transition-duration: 1s";
            for(let elem of whatPeopleSayCircles){
                if(elem.dataset.person === "jane"){
                    elem.className = "section-what-people-say-image-link-circle"
                    elem.style = "margin-bottom: 0; transition-duration: 1s";
                }
            }
            for(let elem of whatPeopleSayLinks){
                if(elem.dataset.person === "hasan"){
                    elem.className = "section-what-people-say-image-link section-what-people-say-image-link-active"
                    elem.style = "top: 5px; transition-duration: 1s"
                    elem.previousElementSibling.className = "section-what-people-say-image-link-circle section-what-people-say-image-link-circle-active";
                    elem.previousElementSibling.style = "margin-bottom: 40px; transition-duration: 1s";
        
                }
            }

            whatPeopleSayImage.src = "./images/Section what people say/Layer 1.png"
            personName.innerHTML = "Hasan Ali";
            personPosition.innerHTML ="UX Designer"
            whatPeopleSayDescription.innerHTML = "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis."

            break
        }
    }
}


whatPeopleSayRightButton.addEventListener("click", onWhatPeopleSayRightButtonHandler);